#!/usr/bin/env python
from __future__ import print_function, absolute_import, division

import logging
import json

from traceback import print_exc
from errno import ENOENT
from stat import S_IFDIR, S_IFREG
from sys import argv, exit
# from time import time
time = lambda: 0
from pprint import pprint

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn, fuse_get_context

def get_attr(path, frag):
    try:
        folder = True
        for part in path.split('/')[1:]:
            old = frag
            frag = frag[part]
            folder = isinstance(frag, dict)
        if folder:
            return dict(st_mode=(S_IFDIR | 0o777), st_nlink=1+len(list(x for x in old.keys() if isinstance(old[x], dict))))
        else:
            size = len(frag)
            return dict(st_mode=(S_IFREG | 0o777), st_size=size, st_nlink=1)
    except Exception as e:
        print_exc
        raise FuseOSError(ENOENT)

def get_dir(path, frag):
    try:
        folder = True
        for part in path.split('/')[1:]:
            frag = frag[part]
            folder = not isinstance(frag, dict)
        return frag.keys()
    except:
         raise FuseOSError(ENOENT)

def get_content(path, frag, offset, size):
    try:
        folder = True
        for part in path.split('/')[1:]:
            old = frag
            frag = frag[part]
            folder = isinstance(frag, dict)
        return frag[offset:offset + size]
    except Exception as e:
        print_exc
        raise FuseOSError(ENOENT)

def set_content(path, frag, buf, offset):
    try:
        folder = True
        for part in path.split('/')[1:]:
            old = frag
            key = part
            frag = frag[part]
        old[key] = old[key][:offset] + buf
    except Exception as e:
        print_exc
        raise FuseOSError(ENOENT)

def add_file(path, frag):
    try:
        folder = True
        for part in path.split('/')[1:]:
            old = frag
            key = part
            if part in frag.keys():
                frag = frag[part]
        old[key] = ""
    except Exception as e:
        print_exc
        raise FuseOSError(ENOENT)

def mv(old_path, new_path, frag):
    files = frag
    try:
        for part in old_path.split('/')[1:]:
            old = frag
            key = part
            frag = frag[part]
        value = frag
        old_old = old
        old_key = key
        del old[key]
        frag = files
        i = 0
        for part in new_path.split('/')[1:]:
            old = frag
            key = part
            if part not in frag.keys():
                i += 1
                if i > 1:
                    raise Exception('No path')
            else:
                frag = frag[part]
        old[key] = value
    except Exception as e:
        print_exc()
        old_old[old_key] = value
        raise FuseOSError(ENOENT)


def mkdir(path, frag):
    files = frag
    try:
        i = 0
        for part in path.split('/')[1:]:
            old = frag
            key = part
            if part not in frag.keys():
                i += 1
                if i > 1:
                    raise Exception('No path')
            else:
                frag = frag[part]
        if i != 1:
            raise Exception('File exists')
        else:
            old[key] = {}

    except Exception as e:
        print_exc()
        raise FuseOSError(ENOENT)


class Dict(LoggingMixIn, Operations):

    def __init__(self, d={}):
        self.files = d

    def getattr(self, path, fh=None):
        if path == '/':
            st = dict(st_mode=(S_IFDIR | 0o777), st_nlink=2+len(list(x for x in self.files.keys() if isinstance(self.files[x], dict))))
        else:
            st = get_attr(path, self.files)
        st['st_ctime'] = st['st_mtime'] = st['st_atime'] = time()
        return st


    def readdir(self, path, fh):
        if path == '/':
            ct = self.files.keys()
        else:
            ct = get_dir(path, self.files)
        ct += ['.', '..']
        return ct

    def read(self, path, size, offset, fh):
        return get_content(path, self.files, offset, size)

    def write(self, path, buf, offset, fh):
        print(buf)
        set_content(path, self.files, buf, offset)
        return len(buf)

    def create(self, path, mode):
        add_file(path, self.files)
        return 0
    def rename(self, old, new):
        mv(old, new, self.files)

    def mkdir(self, path, mode):
        mkdir(path, self.files)

    # Disable unused operations:
    access = None
    flush = None
    getxattr = None
    listxattr = None
    open = None
    opendir = None
    release = None
    releasedir = None
    statfs = None


if __name__ == '__main__':
    if len(argv) != 2:
        print('usage: %s <mountpoint>' % argv[0])
        exit(1)

    logging.basicConfig(level=logging.DEBUG)
    try:
        d = json.load(open(argv[1]+'.dump', 'r'))
    except:
        print_exc()
        d = {}
    fuse = FUSE(Dict(d), argv[1], foreground=True, ro=False)
    json.dump(d, open(argv[1]+'.dump', 'w'), indent=4)
